# Contents #

1. What is biomechanics
2. Two main areas of Biomechanics
* We will look at what biomechanics is

### What is biomechanics ###
* It is the study of forces acting on the body and body segments and the consequences of those forces
    * Consequences of force 
        * Movement 
        * Growth 
        * Injury

### Two main areas of Biomechanics ###
* Performance improvement
    * Training and technique
    * Equipment
* Injury Prevention

## Summary ##
Ultimate goal of Biomechanics is to enhance performance