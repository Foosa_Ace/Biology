# Contents #
1. Creatine Phosphate 
2. Anaerobic Respiration
2. Aerobic Respiration
4. Oxygen Debt
5. Muscle fatigue
* We will cover muscle metabolism and the ways in which muscles source their ATP.

## Muscle Metabolism ##
* Ways in which muscles source their ATP
* ATP
    * Allows Muscle Contraction
    * Active Transport Pumps
* Amount of ATP within muscle fibers for muscle contraction is only sufficient for few seconds so muscle fibers require other ways to source the ATP
* Muscle fibers source ATP in 3 ways:
    * Creatine Phosphate
    * Anaerobic cellular respiration
    * Aerobic cellular respiration
    * Oxygen debt

## Creatine Phosphate ##
* Molecule which can store energy within its phosphate bonds
* Whilst the muscle is relaxed they produce more ATP then required for resting metabolism
* This excess ATP transfers a phosphate group to Creatine producing (Creatine phosphate + ADP) this acts as a very quick energy reserve for creating ATP
* When a muscle requires ATP creatine phosphate transfers its phosphate group back to ADP which is catalyzed by the enzyme creatine kinase to form (Creatine + ATP)
* This ATP can be then used to create muscle contraction, this form of source is quick and lasts for ~15 seconds due to finite amounts of creatine (100m race)

## Anaerobic Respiration / Glycolysis ##
* As creatine phosphate is depleted the muscle sources from glycolosis
* Anarobic process by which glucose is broken downn to produce ATP in a series of 10 steps
* Cannot generate ATP as fast as creatine phosphate
* Glucose used in Glycolysis can be gained through 
    * Blood glucose: Diffuses straight into the muscle
    * Muscle glycogen: Within the muscle tissue
        * Polysaccharide: Has tons of glucose molecules stuck together(30k)
        * Must undergo glycogenolysis to make glucose available for glycolysis
* Glycolysis produces
    * ATP x 2 molecules
    * Pyruvates x 2 molecules
* Pyruvates: When we have no oxygen available it's broken down from pyruvate into lactic 
    * Pyruvate(NADH + H<sup>+</sup>) <--> Lactate(NAD<sup>+</sup>) catylized by lactate dehydrogenase enzyme
    * The job of lactate dehydrogenase is to take the hydride group from one molecule and put it on to another
    * The importance of NAD<sup>+</sup> (Nicotinamide adenine dinucleotide) is that it is the only thing that keeps glycolysis going
* Because this process is inefficient as it makes two ATP molecules per glucose so after a minute anerobic respiration is depleated (400m race)

## Aerobic respiration ##

* If during the glycolysis process during the production of the pyruvate we have oxygen this pyruvate enters aerobic respiration.
* Breakdown of glucose or other nutrients in the presence of oxygen to produce CO<sub>2</sub>, H<sub>2</sub>O, and ATP
* Inputs for aerobic reespiration are:
    * Pyruvic acid: Produced off glycolysis 
    * Fatty acids: When we are low on glucose and glycogen then the body turns to fat to make fatty acids and glycerol, these can enter aerobic respiration
    * Amino acids: When protien is high and glucose and fat are low the above can be broken down to amino acids then used in aerobic respiration
* All the above process occurs in the mitochondria in the presence of oxygen in the form of hemoglobin(blood) or oxygen attached to myoglobin(protien within the muscle fiber that binds up oxygen) 
* The above stuff can be used for aerobic respiration so this occurs in a two step process within the mitochondria
    * Kreb's cycle/TCA/Cytric acid cycle
    * Enter the electron transport system 
* The outcome is to yeild =  CO<sub>2</sub>, H<sub>2</sub>O, and ATP(much more efficient than glycolysis)
* 36 ATP per glucose molecule and 100 per fatty acid molecule 
* Requires steady stream of oxygen(walking or jogging)

## Oxygen debt ##

* Oxygen required to compensate for the ATP which we have had to produce without oxygen 
    1) Convert Lactic acid into glucose of glycogen stores which can then be put into the liver
    2) Resynthesize Creatine to creatine phosphate 
    3) Replace the oxygen that's been removed from myoglobin in our muscles to oxymyoglobin

## Muscle fatigue ##
* We don't really know yet but some are:
1) Lactic acid build up
2) Leaking of calcium out of the sarcoplasm which affects contractivity 
3) Failure of action potentials to release enough acetylcholine at the neuromuscular junction

## Summary ##
Today we saw:

* ATP is abundant in a resting muscle when combined with creatine forms creatine phosphate which can then be give it's phosphate group to ADP and make the ATP  
* Glucose from the blood or muscle under glycolysis produces 2 x ATP and 2 x Pyruvate then lactic acid
* If we have oxygen we then use the pyruvate, fatty acid, amino acid which undergoes the kerbs cycle to form ATP 
* After exercise we have to repay our oxygen debt that oxygen is used to reproduce
    * Glucose/Glycogen
    * Creatine Phosphate
    * Oxymyoglobin 


## Notes ##
[Episode 5 of PhysiologyZone](https://youtu.be/kBSklme_O4c)
* **NEED MORE NOTES ON Muscle fatigue**

#### Questions ####
* Can creatine be increased without supplementation?
* Recharge time for creatine 












