# Contents #
1. Motor Neuron
2. The Neuromuscular Junction
* Covers how the neuromuscular junction and the events by which an action potential reaches and then passes into a muscle 

## Motor Neuron ##
![Motor Neuron Pathway](https://open.oregonstate.education/app/uploads/sites/48/2019/07/1212_Sensory_Neuron_Test_Water_revised-copy-e1568245696709-1024x596.png "Motor Neuron Pathway")

* Action potential for muscle contraction begins in the motor cortex of the brain
* Reflexes are automatic and they occur at the level of spinal cord
* **Action potential path:**
    1) Brain
    2) Spinal cord via tracts(nerve fibers)
    3) Motor neuron
    4) Axon terminals
    5) Skeletal Muscles
* **Motor neuron is made up of:**
    * Cell body
    * Axon: Wrapped in myelin sheath which allows conduction to happen quickly
* Many motor neurons and sensory neurons make up a nerve in this case the musculocutaneous nerve this nerve innervates our bicep muscles

### Motor Neuron Zoomed ###
![Motor Neuron Zoomed](https://s3-us-west-2.amazonaws.com/courses-images-archive-read-only/wp-content/uploads/sites/18/2014/07/19181606/1009_Motor_End_Plate_and_Innervation.jpg "Motor Neuron Zoomed")

* One axon terminal goes to one muscle fiber at the end we have synaptic end bulb or bouton
* Each bouton attaches to the center of skeletal muscle fiber this allows the whole muscle fiber to recieve the action potential and contract almost simultaniously
* The site where the synaptic end bulb and the sarcolemma meet is called the Neuromuscular junction

## Neuromuscular Junction ##
How the action potential reaches and innervates the muscle and allowing it to contract

### Components of Neuromuscular junction ###
* Cytosol: It is the liquid found inside cells.
* Vesicles: Contain Acetylcholine
* Synaptic cleft: Interstitial fluid
* Sarcolemma: Motor end plate

### Processes that occur to create a muscle action potential and contraction ###
* Processes that occur to create a muscle action potential and then contraction can be split into 5 parts:
1) Get the action potential through the neuron, reaching the synaptic end bulb causing depolarization, this causes opening of calcium channels and influx of calcium into synaptic end bulb
2) Due to influx of calcium the vesicles fuse and then release Acetylcholine into the synaptic cleft this is called exocytosis
3) Similar to locks and keys, Nicotinic Acetylcholine receptors are like locks and Acetylcholine are like the keys when to molecules of Acetylcholine bind to the Nicotinic Acetylcholine receptors it causes conformational change in the structure 
4) Once the Nicotinic Acetylcholine receptors are open they only allow sodium and potassium ions
    * Sodium is the most abundant extracellular cation(we got lot of this stuff)
    * Potassium is the most abundant intracellular cation
Sodium flows into the muscle fiber, some potassium leaks out but the sodium flux is high. This causes change in the membrane potential - charge of the outside vs inside of the muscle fiber. This change in membrane potential triggers a depolarization called end plate potential within the end plate
5) Action potential goes from sarcolemma depolarization down through the T tubules to the Triad zone this causes relase of calcium in the Sarcoplasmic reticulum 


### Analysis of Myofibril ###
![Zoomed Myofibril](Images/zoomed-myofibril.png)
**Analysis of myofibril on how it releases calcium**
* Sarcoplasmic reticulum contain lots of calcium
* Dihydropyridine Receptor: Made up of tetrad(4 L-type calcium channels) 
* Ryanodine Receptor: Made up of calcium release channel with 4 foot processes

1) When the calcium arrives into the T tubule it causes conformational change in the dihydropyridine receptor, this creates a mechnical coupling between 4 Dihydropyridine Receptors and the 4 Ryanodine Receptors
2) This mechanical coupling causes conformational change in the calcium release channels that allows calcium from the sarcoplasmic reticulum
3) The calcium moves into the sarcoplasm this activates troponin C on the tropomyosin and that leads to muscle contraction

#### Calcium induced calcium release ####
* Another process for relase of calcium into the sarcoplasm
* The calcium relased induces the Ryanodine Receptor into further relase of calcium
* This is a contributary to the mechanical coupling

### Summary ###
1) Calcium release: Action potential
2) Exocytocis: Vesicles fuse
3) Nicotinic Acetylcholine receptors activation: Acetylcholine diffuses
4) Sarcolemma depolarisation: Sodium influx
5) Calcium release: Sarcoplasmic reticulum

#### Termination of the acetylcholine activity ####
* Acetylcholine's effect of producing muscle fiber action potential and contraction only lasts briefly due to the endzyme Acetylcholinesterase
* Acetylcholinesterase exists within *synaptic cleft*
* This breaks down Acetylchloine into, the below, and they cannot independently activate the acetylcholine receptor
    * Acetyl
    * Chloine

### Recreation of Acetylcholine ###
* Acetylcholine is recreated in the *synaptic end bulb* by
    * Acetylcholine A
    * Choline
* The above is catalyzed by: Choline Acetyltrannsferase

The above steps repeat for every other action potential that reaches the neuromuscular junction

# Summary #
Stuff covered today: 

#### Motor neuron ####
1) Cell body
2) Axon
3) Axon Terminals
4) Synaptic end-bulb 
5) Sarcolemma

#### Neuromuscular Junction ####
Action Potential is generated
1) Ca release
2) Exocytosis
3) Ach Receptor activation
4) Sarcolemma Depolarization
5) Ca release

## Notes ##
* [Episode 3 of PhysiologyZone](https://www.youtube.com/watch?v=kIBKbg5eNCU) 














