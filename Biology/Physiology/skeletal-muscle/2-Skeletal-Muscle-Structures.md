# Contents # 
1. Macroscopic features of skeletal muscle
2. Microscopic features of skeletal muscle
* We look at the structures of skeletal muscle macroscopically and microscopically

### Animations ###
* [Whole lesson summed up in this animation](https://blausen.com/en/video/organization-of-skeletal-muscle/)

![Cool pic](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6f/Blausen_0801_SkeletalMuscle.png/527px-Blausen_0801_SkeletalMuscle.png)


#### New words ####

1) Superficial Fascia: Regular connective tissuue. Seperates skin from muscles, composed of areolar connective tissues and adipose tissue.
2) Deep Fascia: Irregular connective tissue. Lines body walls, wraps and holdes muscles of similar functions together.
3) Adductors: A muscle whose contraction moves a part of the body.
4) Tendon: Attach muscles to bones.
5) Aponeurosis: Broad flat tendon sheet.
6) Sarcolemma: Underneath the endomysium cell membrane which wraps all the muscle fibers
7) Transverse tubules: (Vias) Extensions of the cell membrane that penetrate into the centre of skeletal and cardiac muscle cells.
8) Interstitial fluid: Fluid found in the spaces around cells. It helps bring oxygen and nutrients to cells and to remove waste products from them.
9) Action potential: The change in electrical potential associated with the passage of an impulse along the membrane of a muscle cell or nerve cell.


## Macroscopic Features ##
Muscle layers: Tendon, Deep Fascia, Muscle Belly

Connected tissue layers:
1) Epimysium surrounds Muscle
2) Perimysium surrounds Fascicle
3) Endomysium surrounds Muscle fiber

* These three layers extend to form tendons whuch attach muscles to bones.

## Microscopic Features ##

* **Cool Muscle Fiber/Muscle cells facts:**
1) 10-100 Micrometer
2) 10cm Length
3) No. of muscle fibers are predetermined before growth
4) Muscle grow by Hypertrophy - size and not by Hyperplasia - number
5) Muscles can be lost due to injury or exercise and to repair them we have satellite cells like stem cells for muscles

* **Myofibril**
1) Underneath the sarcoplasmic reticulum but not in direct contact.
2) Smaller units within the muscle cell. They are like little pieces of string.
3) Units that cause contraction

* **Sarcolemma**
1) Underneath the endomysium we have a cell membrane called Sarcolemma
2) It wraps all the muscle fibers

* **Nuclei**
1) Underneath the sarcolemma 
2) Makes proteins for contraction


* **Sarcoplasm other cell cytoplasm**
1) Glycogen: Glucose polymer which creates ATP
2) Myoglobin: Protien and is only a muscle which binds oxygen in the surrounding fluid and then releases as needed.
3) Mitochondria: Generates the ATP

* **Transverse Tubules**
1) Center of muscle fiber, wrapping around each myofibril
2) Open at the top, contains interstitial fluid in them this allows electrical current aka action potential
3) Similar to Vias

* **Sarcoplasmic reticulum**
1) Series of sacs around Myofibril
2) Dilated at the ends

* **Terminal Cisternae**
1) Dilated part of Sarcoplasmic reticulum in contact with T tubules 
2) Triad zone: Termninal cisterna + T tubule + Termninal cisterna

* **Triad Zone**
1) Area where two Terminal cisternae and T tubule meet
2) Allows the action potential from the T tubule to all across the miofibrils

* **Sarcoplasm**
1) Surrounds all the myofibrils
2) Cell fluid of muscle cells in other cells cytoplasm
3) Within sarcoplasm three key things that allow muscles to contract
    * Glycogen: Glucose polymer used to create energy in the form of ATP
    * Myoglobin: Protein which is only a muscle which binds oxygen in the surrounding fluid and then releases it as needed to the next structure which is the Mitochondria
    * Mitochondria: Lie in rows in the muscle fibers, generate ATP for muscular contraction.

* **Sarcomere**
1) Basic functional contractile unit of the myofibril
2) Series of structual repeating units which make up the myofibril
3) Structure: Layered in 2:1 ratio
    * Thin Filament: Contains Z-discs, I-bannd
    * Thick Filament: Contains the A-band
4) Z-discs: Dense material which seperates one sarcomere form another
5) A-band: Thicker middle part of the sarcomere (Anisotrophic to polarized light)
6) I-band: Continues until another A-band (Isotrophic to polarized light)
7) M-line: Middle part of the A-band
8) H-zone: Portion of thick filament not overlapped by thin filament (Heller/Bright)

* **Proteins**

1) Contractile Proteins: Actin(Thin) & Myosin(Thick)
2) Regulatory Proteins: Actin[Tropomyosin, Troponin]
3) Structural Proteins: Actin[Nebulin]; Myosin[Titin, Myomesin]: these maintain the shape and size of the muscle 

* **Myosin**
1) Predominant part of the thick filament
2) Shaped like two golf clubs twisted
3) They point towards the M-line

* **Actin**
1) Prodominant part of thin filament
2) Anchored to the Z-disc, Twisted into heliux formation
3) On each actin we have a myosin binding site(hole) which is for the myosin head during contraction

* **Tropomyosin**
1) On Actin
2) Types: This holds the tropomyosin in place
    * Troponin C
    * Troponin I
    * Troponin T
3) Regulate when a muscle contracts
    * In a relaxed state if we don't want contraction it does this by blocking the myosin binding heads on the actin
    * When contraction happens it moves out of the way to allow the actin

* **Titin**
1) Third most abundant protein behind actin and myosin
2) Stablize the position of the thick filament by anchoring it to the M-line and the Z-disc
3) Very flexible can stretch up to 4 times it's size

* **Myomesin**
1) Forms the M-line

* **Nebulin**
1) Non elastic protein that wraps around the thin filament 
2) Keeps the shape of the thin filament and it's anchorage too the Z-disc

* **Dystrophin**
1) Reinforcing Sarcolemma
2) Has a link with Duchenne muscular dystrophy
    * Auutosomial dominant X-linked form of muscular dystrophy - only Males
3) If the stablity of this protein is lost then the stablity of Sarcolemma is lost
4) Leads to death of muscle fibers and replacement with adipose and connective tissue

## Notes ##

[Episode 2 of PhysiologyZone](https://youtu.be/o3eVnWBpDOQ)

#### Questions ####
* Muscle repair and satellite cells?
* Muscle hypotrophy?

#### Resources ####
* [Cool animation](https://blausen.com/en/video/organization-of-skeletal-muscle/)
* [Great site - Kenhub](https://www.kenhub.com/)
