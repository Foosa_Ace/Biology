# Contents #
1) Function of muscle tissue
2) Types of muscle tissue
3) Properties of muscle tissue
* Basic overview of the muscular system.

### Function of muscle tissue ###
1) Produce body movement
2) Continual contraction (stablize position)
3) Storing and moving substances in our body
4) Thermoregulation (to generate heat)

### Types of muscle tissue ###
1) Skeletal Muscles - tendon to bones
2) Cardiac Muscles - found in heart
3) Smooth Muscles - hollow organs

### Properties of muscle tissue ###

1) **Skeletal Muscles:**
* Regular cylindrical fibers
* Many Peripheral Nuclei
* Striated

2) **Cardiac Muscles:** 
* Branched Cylindrical Fibers
* Intercalated discs
* Single Central Nucleus
* Striated

3) **Smooth Muscles - hollow organs**
* Elliptical shaped fibers
* single central nucleus
* Not striated

4) **Common properties**
* Electrical Excitablity - Create electrical signals in response to stimulus
* Contractility - Actin, Myosin
* Extensiblity - Stretch without damage
* Elasticity

## Notes ##

* [Muscle song](https://www.youtube.com/watch?v=hY2fa6Q98-k)
* [Episode 1 of PhysiologyZone](https://www.youtube.com/watch?v=vaIYWhCdyu8)