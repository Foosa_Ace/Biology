# Contents #
1. Muscle Contraction (Sliding Filament Theory)
2. Length-Tension Relationship
* Covers the process of muscle contraction. We will also look at the length tension relationship due to the intimate relationship this has to muscle contraction

## Muscle Contraction ##
[Episode 4](https://www.youtube.com/watch?v=xktSUkf4BGc)
* Contraction cycle
    * Excitation-Contraction Coupling
* Length tension relationship

### Historical Note ###
* Prior to the 1950s people thought muscles contracted by folding and straightning process
* In 1954 two independent teams published papers in Nature
* Fundamentally the two teams showed that in a contracted and relaxed state the lengths of the thick and thin filaments within the myofibril remained the same
* What was actually happening was they were sliding over one another, this explanation is known as the *Sliding filament theory*

![Muscle contraction zoomed](Images/muscle-contraction.png)
### Contraction cycle ###
Describes the steps by which stirated muscles(skeletal and cardiac) contract. The movement of calcium into the sarcoplasm that leads to the begining of the contraction cycle

1) Calcium binds to troponin C: This causes conformational change in troponin to slide the tropomyosin off of the binding sites on the actin molecules
2) Cross bridge formation: Myosin has Adenosine Di-Phospate(ADP) and Inorganic phospate(Pi) the presence of this phospate group phosphorylates(addition of phosphate) the myosin head that causes conformational change so then it attaches to actin at it's myosin binding sites and forms cross bridges 
3) Power stroke: Causes movement, the release of ADP and Pi from the myosin head causes a conformational change so that the myosin head bends at 45° and this pulls the actin molecule towards the M-line (center of the sarcomere)
4) ATP binding: The ATP binds on the actin binding site and this releases the myosin from the actin
5) ATP hydrolysis: Myosin head also has ATPase enzyme this hydrolyzes the ATP to ADP and Pi and this releases the myosin head in to the initial position("cocked" position)

The above process repeats itself as long as
    * There's enough ATP 
    * Calcium concentration near the thin filament is high

Cool facts
* There are 500 crossbridges per thick filament 
* These attach and detach 5 times a second

Once the stimulus has ceased calcium moves out of the sarcoplasm in the following manner:
1) Active transport pumps(sarcolemmal sodium calcium exchanger?) on the sarcoplasmic reticulum which use ATP and then hydrolyzed to ADP and Pi which then open the pumps 
2) Once open they pump back the calcium back out of the sarcoplasm into the sarcoplasmic reticulum
3) Within the sarcoplasmic reticulum a protein called calsequestrin this binds up 20 molecules each and that's how so much calcium is stored

### Length tension relationship ###
![Length-tension](Images/length-tension.png)

Length-tension relationship states that isometric tension(without change in length) generation in skeletal muscle is a function of the magnitude of overlap between actin and myosin filaments

Forcefulness of the muscle contraction depends on the length of the sarcomeres before contraction occurs, muscle length is linked to the tension it can develop  
* Optimal lenght for maximum contracting power is 2.0-2.4 um

## Notes ##
* Needs more stuff from biomechanics 

### Questions ###
Is calcium pumped back to the sarcoplasmic reticulum through ryanodine receptors or sarcolemmal sodium calcium exchanger









