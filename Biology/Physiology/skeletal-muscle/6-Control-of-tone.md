# Contents #

1. Motor Unit
2. Twitch Contraction
3. Quantal and Frequency Summation
4. Resting Muscle Tone and Movement
* We will look at the control of tone

### New words ###
1) **Muscle tone:** Muscle tone (residual muscle tension or tonus) is the continuous and passive partial contraction of the muscles, or the muscle's resistance to passive stretch during resting state. Even when a muscle is at rest, there is random firing of motor units. This random firing is known as muscle tone.


## Motor unit ##
Motor unit is the number of muscle fibers innervated by one motor neuron
* Eye muscle such as the lateral rectus muscle have 10-15 muscle fibers supplied by one motor neuron - Small size of motor unit; Large number of motor units. This allows precise movement. Similar for the muscles within the hand.
* In contrast for a biceps muscle, 1000+ fibers are all stimulated by a single motor neuron. Large size of motor units; Small number of motor units. This allows gross movement. 

### All or none law ###
* Action potential are always the same size in any given neuron, in any given muscle fiber and it innervates ALL of muscle fibers
1) When a motor neuron synapses with a muscle fiber and a single action potential comes down the motor neuron it will supply single action potential to all the muscle fibers it supplies
2) Inervates ALL muscle fibers
* Fiber contraction in terms of speed and strength does vary and the key things which determine this are the Twitch Contraction and Quantal and Frequency Summation

## Twitch contraction ##
![Twitch contraction](https://content.byui.edu/file/a236934c-3c60-4fe9-90aa-d343b3e3a640/1/module7/images/1012_Muscle_Twitch_Myogram.jpg)

* This is the brief contraction of all the muscle fibers in a motor unit in response to a single action potential down its motor neuron
* On the graph we see what happens when a single stimulus, i.e. action potential stimulates a muscle fiber. 
* The tension increases in the muscle and then it decreases again down to resting phase.

1) **Latent phase**: The muscle action potential sweeps across the sarcolemma and calcium ions are released from the sarcoplasmic reticulum into the sarcoplasm this process lasts 2 to 5 ms
2) **Contraction phase**: Calcium binds to the troponin C it reveals the myosin binding sites on the actin and the cross bridges are formed this lasts about 10 to 100 ms
3) **Relaxation phase**: The calcium is activly transported back into the sarcoplasmic reticulum, the myosin binding sites are covered by tropomyosin complex, the myosin detaches from the actin and the tension in the muscle fiber decreases. This lasts 10 to 100 ms
4) **Refractory period**: During the latent period a second stimulus will not cause a second contraction basically the muscle fiber is too busy to receive another action potential
* The graph on average takes about 100 to 200 ms but it can be as quick as 20 ms the speed depends on the types of skeletal muscle fibers.

### Types of skeletal muscle fibers ###
[Muscle contraction](https://en.wikipedia.org/wiki/Muscle_contraction): Muscle contraction is the activation of tension-generating sites within muscle fibers. READ MORE FROM THE WIKI IT'S GREAT.

1) **Slow oxidative fibers(Type 1):**
* Small in size which causes them to have weak contraction
* They use oxygen and have a lot of blood capillaries so they are dark red
* They have tons of mitochondria and lots of myoglobin to store oxygenn
* Generate the ATP by aerobic respiration so less glycogen reserves
* Twitch lasts the longest of all these types at about 200ms
* Efficient - they don't fatigue very easily 
* Prodominent in muscles used in posture and long distance running
2) **Fast oxidative-glycolytic fibers(Type 2a):**
* Intermediate in diameter and are quite strong
* They have myoglobin blood capillaries and they look dark because of that
* They have mitochondria so use aerobic respiration and also have glycogen stores and use glycolosis these types of fibers use both respiration
* Twitch lasts at about 100ms 
* Utilized in 400 to 800m race, reasonablly efficient 
3) **Fast glycolytic fibers(Type 2b):**
* Large in diameter which causes them to have strong contraction
* Use glycolosis for respiration they have lots of glycogen stores and little myoglobin and few capilaries which makes them look whiter
* Fastest twitch lasting about 50ms
* Best in terms of the amount of energy it can produce and the power of contraction but poor efficiency, they fatigue quickly
* **TRUTH IN REALITY:** The above is just a spectrum and muscle fibers do not fit exactly as above but have charecteristics of these types so there is a lot of overlap in reality, speed of the twitch is the reason for the name.
* The speed is directly correlated with the enzyme ATPase which is directly responsible for hydrolyzing the ATP on the myosin heads to ADP and Pi which is required for forming the cross bridges with actin
* In us there are several different isomers of the ATPase and they all work in different rates
* We are all genetically determined before birth of how much of one fiber type we have in comparision to another
* Posture muscles like back are slow twitch whereas biceps are more fast twitch fibers 

## Quantal and Frequency Summation ##
* **Quantal summation:** Number of motor units which are recruited and the number of muscle fibers contracting 
* Muscles work by recruiting the weakest motor units first, as the weight increases stronger motor units are recruited
* Due to this our muscles don't fatigue too quickly 
* This also allows us to change the force of the contraction which allows more precise movements
* **Frequency summation**: Best explained using an [electromyogram](https://www.apsubiology.org/anatomy/2010/2010_Exam_Reviews/Exam_3_Review/CH_09_Electromyography.htm):

![Frequency summation](https://upload.wikimedia.org/wikipedia/commons/d/d4/1013_Summation_Tetanus.jpg)
* A single stimulus to the muscle fiber peaks in terms of its tension. During the first moment of contracting and building the tension that's the refractory period i.e. You cannot get it to contract again in that period
* As its starting to relax it can recieve another action potential. If the action potential reaches before the muscle is completely relaxed the second contraction gets stronger
* This continues to happen until the muscle developes maximum tension 
* Remember contraction and relaxation period is much longer than the refractory period in the muscle 
* Because the graph looks like a wave it is refered to as wave summation

### Tetany ###
* Occurs when a motor unit is maximally stimulated by its motor neuron
* When a muscle is tetenised the muscle remains at a constant and steady state and that's the maximal possible contraction something like muscle bulging and getting stuck there?

#### Types of tetanus ####
![Types of tetanus](https://www.apsubiology.org/anatomy/2010/2010_Exam_Reviews/Exam_3_Review/MMUR.fig.9.13a.jpg)

* **Incomplete/Unfused:** Stimulus in terms of the action potential is appplied about 30 times per second in this the muscle fiber is producing maximal tension but is slightly relaxing in between the contractions
* **Complete/Fused:** Lets say the stimulus is a 100 times a second and they do not have the time to relax at all 
* **Theory behind all this**
    1. Action potential is *releasing Ca in Sarcoplasm*
    2. *Causes contraction*
    3. Almost instantaniously *Ca begins leaving the muscle fiber*
    4. Muscle *begins to relax*
    5. Now if another action potential hits it there's more Ca release and the muscle fiber cross bridges partially activated are now activated for longer because more intercelullar crossbridges and develop more force
    6. With quicker and quicker action potentials more force is developed by the muscle fiber because it has time to create cross bridges
    7. When all cross bridges are formed maximal tension is reached within the muscle fiber 

* In our body sustained contraction of skeletal muscles are generated by incomplete tetany
* Most believe it's because of asynchronous activation of our motor units whereby motor units which are relaxed begin to fire and the motor units which are contracted begin to relax and they cycle
* This can be practically seen when we hold our hands we have fine tremors which is more marked when held something heavy

### Treppe effect/Bowditch effect ###
![Treppe effect](https://i.pinimg.com/originals/78/9b/cd/789bcd6ae1d66ddc16c2ab11ca54974c.jpg)
* This is not exactly not how a muscle generates a graduated contraction but this is a phenomenon that's demonstrated by all muscle fibers and it occurs as follows: 
    1. Let's say the muscle is cold and inactivated when its initially stimulated we get a release of calcium and that causes the cross bridges to form in the muscle and it only activates a skeletal muscle for about 10ms where as a muscle contraction takes about a 100ms then the action potential decays and the calcium starts to flow back into the sarcoplasmic reticulum and it relaxes
    2. Even at a relaxed state when stimulated it will contract with a greater force than before this is because even at relaxed state all the intracellular calcium cannot go back into the sarcoplasmic reticulum at a short period of time so it has more intracellular calcium present than before hence it can create a greater force when another calcium potential arrives
    3. This can happen up to about 25% of the maximal tensio that we are able to develop in that muscle assuming the intravel is regular let's say 1 second
* This effect looks like a staircase (Treppe = staircase in German)
* Other elements affecting
    * Changes to the ion gates
    * Enzymes which become more efficient with use 
    * Rising temperatures 
* Warming up process in the gym

## Muscle tone and Movement ##

### Muscle tone ###
* At rest we always have a degree of tone within the muscles this is due to weak and involuntary contraction or twitches of small groups of motor units in each muscles. This is reguated by the brain and the spinal cord which always output action potenntials
* The muscles don't fatigue because the recruitment of small motor units is asynchronous so it's constantly alternating in active and inactive state this is important in skeletal muscles because it allows us to keep upright also important in smooth muscle and digestive muscles
* **Hypotonicity:** Condition characterized by the presence of a lesser degree of tone or tension. We lose tone and muscles are said to be flaccid(saggy and loose)
    * If there's a physical damage to the peripherial nervous system 
    * Change in electolytes like calcium and sodium
* **Hypertonicity:** Condition characterized by the presence of a greater degree of tone or tension(stiffnenss). Due to damage of the central nervous system. If you have a stroke or any damage to the CNS one ends up with this 

### Muscle Movements ###
1. **Isotonic contraction:**
    * Concentric: If enough force is generated in the muscle that's trying to overcome the resistance(weight of the object) the muscle shortens and the structure moves 
    * Eccentric: If the force is greater than that generated by the muscle the it lengthnes despite contracting and it does it in a controlled manner
2. **Isometric contraction:** When the muscle force and the resistance is equal hence the muscle is unable to shorten or lengthen but is still contracting
    * Isometric contractions are important in maintaining posture or stabilizing a joint
* Strength training, involving both concentric and eccentric contractions, appears to increase muscle strength more than just concentric contractions alone. However, eccentric contractions cause more damage (tearing) to the muscle resulting in greater muscle soreness
* Muscle size is determined by the number and size of the myofibrils, which in turn is determined by the amount of myofilament proteins. Thus, resistance training will induce a cascade of events that result in the production of more proteins. Often this is initiated by small, micro-tears in and around the the muscle fibers. If the tearing occurs at the myofibril level the muscle will respond by increasing the amount of proteins, thus strengthening and enlarging the muscle, a phenomenon called hypertrophy. This tearing is thought to account for the muscle soreness we experience after a workout. As mentioned above, the repair of these small tears results in enlargement of the muscle fibers but it also results in an increase in the amount of connective tissue in the muscle. When a person "bulks up" from weight training, a significant percent of the increase in size of the muscle is due to increases in the amount of connective tissue. 
* It should be pointed out that endurance training does not result in a significant increase in muscle size but increases its ability to produce ATP aerobically.

## Summary ##
Today we looked at:
* **Motor Unit**

| Motor neuron | Muscle fibers  |    Type    |  
| ---          |  ------        |---------:  |
| Small size   | Large No.      | Precision  | 
| Large size   | Small No.      | Gross      | 

* **Twitch contraction**
    * Single action potential provides brief contraction
    * Type 1; Type 2a; Type 2b muscle fibers
* **Quantal and frequency summation**
    * Quantal is all about recruitment of motor units
    * Frequency or wave summation
    * Incomplete tetany - way most of our muscles contract
    * Complete tetany
    * Treppe effect
* **Muscle Tone**
    * Hypotonicity: Effect of damage of PNS
    * Hypertonicity: Effect of damage of CNS
* **Muscle movement** Different types of contractions:
    * Isotonic contraction
    * Isometric contraction

## Notes ##
* [Episode 5 of PhysiologyZone](https://www.youtube.com/watch?v=lcj36RYvd1s)

#### Questions #####
* How do we explain the smooth continued movement of our muscles when they contract and move bones through a large range of motion
    * The answer lies in the ordering of the firing of the motor units. If all of the motor units fired simultaneously the entire muscle would quickly contract and relax, producing a very jerky movement. Instead, when a muscle contracts, motor units fire asynchronously, that is, one contracts and then a fraction of a second later another contracts before the first has time to relax and then another fires and so on. [More.](https://content.byui.edu/file/a236934c-3c60-4fe9-90aa-d343b3e3a640/1/module7/readings/muscle_twitches.html)
* Can we build different muscle fiber types?
* Increase intracelluular calcium


