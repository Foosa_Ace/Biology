# How are muscles named? #

# Contents #
* Shape
* Size
* Muscle fiber orientation
* Action
* Number of origins
* Function
* Location

## Shape ##
1. The muscles are named based on how they look like:
* Deltoid muscle, which looks like a triangle is similar to the greek letter delta 
* Trapezius muscle, which has a diamond shape or trapezoid shape 
* platysma muscle, plat which means flat in french from that we get platysma muscle that covers the superficial aspect of the neck
* Serratus anterior, which looks like a saw compared to the latin word serrare

## Size ##
1. Qualities such as length are used to name muscles:
* Vastus = great; Vastus lateralis
* Major = used to describe how large a muscle is compared to another muscle; Pectoralis major
* Minor = muscle smaller to a similar muscle; Pectoralis minor
* Maximus = largest or greatest; Gluteus maximus is the largest muscle in the pelvic region
* Minimus = least or smallest; Gluteus minimus is the smallest of three gluteal muscles; Flexor digiti minimi which flexes the little finger
* Longus = long; Longus capirits a long muscle of the head and neck
* Brevis = short; Abductor pollicis brevis short muscle of the thumb

## Muscle fiber orientation ##
1. Transverse: muscles which run perpendicular to the midline
2. Oblique: muscles which run diagonally or slanting
3. Rectus: run parallel to the midline rectus in latin means straight

## Action ##
1. Flexion/extension
2. 