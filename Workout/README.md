## Workout ##


[Startup](https://youtu.be/90zaCFYwEpM)
#### A1 ####
* Eccentric pull-ups:- 3-5 reps
* Plank:- 30-40s
* Elevated push-ups:- 8 reps
#### A2 ####
* Eccentric chin-ups:- 3-5 reps
* Ab wheel/Kneeling walkout:- 5-8 reps
* Eccentric dips:- 5-8 reps

https://youtu.be/3MxHX9j15BU
